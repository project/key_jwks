<?php

namespace Drupal\key_jwks\Plugin\KeyProvider;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\key\KeyInterface;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Drupal\key\Plugin\KeyProviderBase;
use Firebase\JWT\JWK;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a key provider that allows RS256 keys to be provided via JWKS endpoints.
 *
 * @KeyProvider(
 *   id = "jwks",
 *   label = @Translation("JSON Web Key Set (JWKS)"),
 *   description = @Translation("The JWKS key provider allows an RS256 public key to be provided by a remote, RFC-7517-compliant JWKS endpoint."),
 *   storage_method = "remote",
 *   key_value = {
 *     "accepted" = FALSE,
 *     "required" = FALSE
 *   }
 * )
 *
 * @noinspection PhpUnused
 */
class JwksKeyProvider extends KeyProviderBase implements KeyPluginFormInterface {

  /**
   * The maximum amount of time that a JWKS can be cached (30 mins).
   */
  protected const MAX_CACHE_AGE = (30 * 60);

  /**
   * The key in the configuration settings for the JWKS endpoint URL.
   */
  protected const CONFIG_KEY_ENDPOINT_URL = 'endpoint_url';

  /**
   * The key in the configuration settings for the ID of the target JWKS Key.
   */
  protected const CONFIG_KEY_KEY_ID = 'key_id';

  /**
   * The logger for this module.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Drupal file system interface.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Drupal time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition) {
    /** @var \Drupal\Core\Logger\LoggerChannelInterface $logger */
    $logger = $container->get('logger.channel.key_jwks');

    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = $container->get('file_system');

    /** @var \Drupal\Component\Datetime\TimeInterface $time */
    $time = $container->get('datetime.time');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $logger,
      $file_system,
      $time
    );
  }

  /**
   * Constructor for JwksKeyProvider.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The unique ID of this plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger for this module.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The Drupal file system interface.
   * @param \Drupal\Component\Datetime\TimeInterface
   *   The Drupal time service.
   */
  public function __construct(array $configuration,
                              string $plugin_id,
                              $plugin_definition,
                              LoggerChannelInterface $logger,
                              FileSystemInterface $file_system,
                              TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger     = $logger;
    $this->fileSystem = $file_system;
    $this->time       = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      self::CONFIG_KEY_ENDPOINT_URL => '',
      self::CONFIG_KEY_KEY_ID       => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form,
                                         FormStateInterface $form_state): array {
    $configuration = $this->getConfiguration();

    $form[self::CONFIG_KEY_ENDPOINT_URL] = [
      '#title'         => $this->t('JWKS URL'),
      '#description'   => $this->t('The URL of the resource that provides the parameters of RSA public keys used to confirm the authenticity of signed JWTs.'),
      '#type'          => 'textfield',
      '#size'          => 100,
      '#maxlength'     => 2048,
      '#required'      => TRUE,
      '#default_value' => $configuration[self::CONFIG_KEY_ENDPOINT_URL],
    ];

    $form[self::CONFIG_KEY_KEY_ID] = [
      '#title'         => $this->t('Key ID (kid)'),
      '#description'   => $this->t('Specifies the ID of the specific public key to return. If omitted, all public keys returned by the resource will be concatenated together in PEM format.'),
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#default_value' => $configuration[self::CONFIG_KEY_KEY_ID],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form,
                                            FormStateInterface $form_state) {
    $form_values  = $form_state->getValues();
    $endpoint_url = $form_values[self::CONFIG_KEY_ENDPOINT_URL];
    $key_id       = $form_values[self::CONFIG_KEY_KEY_ID];

    $local_jwks_path = $this->retrieveUncachedJwks($endpoint_url);

    if (empty($local_jwks_path)) {
      $form_state->setErrorByName(
        self::CONFIG_KEY_ENDPOINT_URL,
        $this->t('The JWKS resource at the specified URL could not be retrieved and saved to a local file.')
      );

      return;
    }

    $jwks_payload = file_get_contents($local_jwks_path);

    if (empty($jwks_payload)) {
      $form_state->setErrorByName(
        self::CONFIG_KEY_ENDPOINT_URL,
        $this->t('The JWKS resource at the specified URL is empty.')
      );

      return;
    }

    try {
      $jwks_decoded =
        \json_decode($jwks_payload, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    catch (\JsonException $ex) {
      $form_state->setErrorByName(
        self::CONFIG_KEY_ENDPOINT_URL,
        $this->t('The JWKS resource could not be parsed as JSON.')
      );

      return;
    }

    $keys = $jwks_decoded['keys'] ?? [];

    if (empty($keys)) {
      $form_state->setErrorByName(
        self::CONFIG_KEY_ENDPOINT_URL,
        $this->t(
          'The JWKS resource at the specified URL did not return JSON containing "keys".'
        )
      );

      return;
    }

    if (!empty($key_id)) {
      $key_found = FALSE;

      foreach ($keys as $key) {
        if ($key['kid'] == $key_id) {
          $key_found = TRUE;
          break;
        }
      }

      if (!$key_found) {
        $form_state->setErrorByName(
          self::CONFIG_KEY_KEY_ID,
          $this->t(
            'The JWKS resource at the specified URL did not return a key having the specified ID.'
          )
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue(KeyInterface $key): ?string {
    $key_value = NULL;

    $config          = $this->getConfiguration();
    $endpoint_url    = $config[self::CONFIG_KEY_ENDPOINT_URL];
    $local_jwks_path = $this->retrieveAndCacheJwks($endpoint_url);

    $logger = $this->getLogger();

    if (empty($local_jwks_path)) {
      $logger->error(
        'The JWKS resource at the configured URL (%url) could not be retrieved and saved to a local file.',
        ['%url' => $endpoint_url]
      );
    }
    else {
      $key_value = $this->readAndDecodeKeys($local_jwks_path);
    }

    return $key_value;
  }

  /**
   * Gets the logger for this module.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The logger.
   */
  protected function getLogger(): LoggerChannelInterface {
    return $this->logger;
  }

  /**
   * Gets the Drupal time service.
   *
   * @return \Drupal\Component\Datetime\TimeInterface
   *   The time service.
   */
  protected function getTime(): TimeInterface {
    return $this->time;
  }

  /**
   * Gets the Drupal filesystem interface.
   *
   * @return \Drupal\Core\File\FileSystemInterface
   *   The filesystem interface.
   */
  protected function getFileSystem(): FileSystemInterface {
    return $this->fileSystem;
  }

  /**
   * Gets the Drupal filesystem URI to the local JWKS cache.
   *
   * @return string
   *   The URI of the local JWKS cache.
   */
  protected function getJwksCacheBaseUri(): string {
    $path = &drupal_static(__METHOD__);

    if ($path === NULL) {
      $path = 'temporary://jwks-cache';

      $dir_writable =
        $this
          ->getFileSystem()
          ->prepareDirectory(
            $path,
            FileSystemInterface::CREATE_DIRECTORY|FileSystemInterface::MODIFY_PERMISSIONS
          );

      if (!$dir_writable) {
        throw new \RuntimeException("Failed to prepare JWKS cache: " . $path);
      }
    }

    return $path;
  }

  /**
   * Retrieves the specified JWKS, caches it locally, and returns its path.
   *
   * If the JWKS is already cached and not stale, the path to the cached copy
   * is returned.
   *
   * If the JWKS cannot be retrieved, an error message is displayed on-screen.
   *
   * @param string $url
   *   The remote JWKS url.
   *
   * @return string|false
   *   Either the local path of the cached JWKS; or FALSE if the JWKS could not
   *   be retrieved.
   */
  protected function retrieveAndCacheJwks(string $url) {
    $local_path = $this->generateLocalCachePath($url);

    if (file_exists($local_path) && !$this->isCachedJwksStale($local_path)) {
      $saved_path = $local_path;
    }
    else {
      $saved_path = $this->retrieveUncachedJwks($url);
    }

    return $saved_path;
  }

  /**
   * Retrieves the specified JWKS, caches it locally, and returns its path.
   *
   * If the JWKS is already cached and not stale, the path to the cached copy
   * is returned.
   *
   * If the JWKS cannot be retrieved, an error message is displayed on-screen.
   *
   * @param string $url
   *   The remote JWKS url.
   *
   * @return string|false
   *   Either the local path of the cached JWKS; or FALSE if the JWKS could not
   *   be retrieved.
   */
  protected function retrieveUncachedJwks(string $url) {
    $local_path = $this->generateLocalCachePath($url);

    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $saved_path =
      system_retrieve_file(
        $url,
        $local_path,
        FALSE,
        FileSystemInterface::EXISTS_REPLACE
      );

    return $saved_path;
  }

  /**
   * Reads in JWKS data from the specified local file and decodes it.
   *
   * If a key ID has been specified in the configuration, only the public key
   * having that identifier is returned in PEM formatted. Otherwise, all of the
   * public keys are concatenated together into a single string.
   *
   * @param string $local_jwks_path
   *   The path where the JWKS data has been cached locally.
   *
   * @return string|null
   *   The public key(s) as a PEM-formatted string; or, NULL if a public key
   *   could not be read.
   */
  protected function readAndDecodeKeys(string $local_jwks_path): ?string {
    $key_str = NULL;

    $logger       = $this->getLogger();
    $jwks_payload = file_get_contents($local_jwks_path);

    $config       = $this->getConfiguration();
    $endpoint_url = $config[self::CONFIG_KEY_ENDPOINT_URL];

    if (empty($jwks_payload)) {
      $logger->error(
        'The JWKS resource at the configured URL (%url) is empty.',
        ['%url' => $endpoint_url]
      );
    }
    else {
      $config = $this->getConfiguration();
      $key_id = $config[self::CONFIG_KEY_KEY_ID];

      try {
        $decoded_jwks =
          \json_decode($jwks_payload, TRUE, 512, JSON_THROW_ON_ERROR);

        $key_resources = JWK::parseKeySet($decoded_jwks);

        if (empty($key_id)) {
          $keys    = array_map([$this, 'convertToPem'], $key_resources);
          $key_str = join("\n", $keys);
        }
        else {
          $public_key = $keys[$key_id] ?? NULL;

          if (empty($public_key)) {
            $logger->error(
              'The JWKS resource at the specified URL (%url) did not return a key having the specified ID (%kid).',
              [
                '%url' => $endpoint_url,
                '%kid' => $key_id,
              ]
            );
          }
          else {
            $key_str = $this->convertToPem($public_key);
          }
        }
      } catch (\JsonException $ex) {
        $logger->error(
          'The JWKS resource at the configured URL (%url) could not be parsed as JSON.',
          ['%url' => $endpoint_url]
        );
      }
    }

    return $key_str;
  }

  /**
   * Determines whether the locally-cached JWKS resource is stale.
   *
   * @param string $local_path
   *   The path to the local copy of the JWKS resource.
   *
   * @return bool
   *   TRUE if the local copy is stale; or FALSE if it is not.
   */
  protected function isCachedJwksStale(string $local_path): bool {
    if (file_exists($local_path)) {
      $request_time       = $this->getTime()->getRequestTime();
      $file_creation_time = filectime($local_path);
      $file_age           = ($request_time - $file_creation_time);

      $is_stale = ($file_age > self::MAX_CACHE_AGE);
    }
    else {
      // Not stale -- doesn't exist.
      $is_stale = FALSE;
    }

    return $is_stale;
  }

  /**
   * Returns the PEM representation of the given OpenSSL key resource.
   *
   * @param resource $asymmetricKey
   *   The OpenSSL key resource for which a PEM-encoded string is desired.
   *
   * @return string
   *   The PEM-encoded version of the key.
   */
  protected function convertToPem($asymmetricKey): string {
    $details = openssl_pkey_get_details($asymmetricKey);

    if ($details === FALSE) {
      throw new \RuntimeException(
        'Failed to get details of key: ' . openssl_error_string()
      );
    }

    return $details['key'];
  }

  /**
   * Generates the deterministic path of the cached copy for a JWKS resource.
   *
   * @param string $url
   *   The URL of the JWKS resource.
   *
   * @return string
   *   The path to the locally-cached JWKS resource.
   */
  protected function generateLocalCachePath(string $url): string {
    $cache_base_path = $this->getJwksCacheBaseUri();
    $local_filename  = str_replace('/', '-', $url);

    return sprintf('%s/%s', $cache_base_path, $local_filename);
  }

}
